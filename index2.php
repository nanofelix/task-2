<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<div>
    <h4>SQL - вызов</h4>
    <pre>
        SELECT
        categories.id as category_id,
        categories.parent_id,
        categories.name as category_name,
        products.id as product_id,
        products.name as product_name
        FROM categories
        LEFT JOIN products ON products.category_id=categories.id
        ORDER BY categories.id, products.id
    </pre>

</div>


<h4>Результат:</h4>



<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);


/* Вспомогательный код, мини орм */
require_once './core/Init.php';
$db = DB::getInstance();
$products = [];


// Делаем запрос с объединением таблиц categories и products
$data = $db->query('
                  SELECT 
                      categories.id as category_id, 
                      categories.parent_id,
                      categories.name as category_name, 
                      products.id as product_id, 
                      products.name as product_name 
                    FROM categories 
                    LEFT JOIN products ON products.category_id=categories.id 
                    ORDER BY categories.id, products.id')
    ->results();


function print_list($categories, $parent = 0)
{
    global $products;


    /* Перебираем список категорий */
    foreach ($categories as $row) {

        /* Если текущая категория является дочерней текущему $parent */
        if ($row->parent_id == $parent) {


            print('<li class="cat">');

            /* Выводим название категории */
            print('<div>' . $row->category_name . '</div>');

            print('<ul>');




            /* Вызываем рекурсивную функцию */
            /* передаем в качестве parent_id,  id текущей категории */
            /* таким образом функция выведет все дочерние категории текущей категории */
            print_list($categories, $row->category_id);


            /* перебираем массив с продуктами */
            /* выводим продукты, относящиеся к текущей категории */
            foreach ($products as $key => $product) {
                if ($row->category_id == $product->category_id) {
                    print('<li><div>');
                    print($product->product_name);
                    print('</div></li>');
                }
            }


            print('</ul>');

            print('</li>');

        }
    }

}


//если запрос в бд вернул хоть одну запись
if (count($data)) {


    /* создаем копию массива, но с уникальными элементами */
    /* уникальность определяется значением поля category_id */
    $categories = array_intersect_key(
        $data,
        array_unique(array_map(function ($item) {
            return $item->category_id;
        }, $data))
    );


    // извлекаем список продуктов из массива
    $products = array_filter($data, function ($item) {
        if (!empty($item->product_id)) return true;
    });


    print('<ul>');

    // Вызываем рекурсивную функцию
    print_list($categories);

    print('</ul>');

}


?>


</body>
</html>





