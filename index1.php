<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
</head>
<body>


<div>
    <h4>SQL - вызовы:</h4>
    <p>
        1) SELECT * FROM categories
    </p>
    <p>
        2) SELECT * FROM products
    </p>
</div>



<h4>Результат:</h4>

<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);


/* Вспомогательный код, мини орм */
require_once './core/Init.php';
$db = DB::getInstance();

$categories = $db->query('SELECT * FROM categories')->results();
$products = $db->query('SELECT * FROM products')->results();


function print_list($categories, $parent = 0)
{

    global $products;

    print('<ul>');

    /* Перебираем все категории */
    foreach ($categories as $row) {

        /* Если текущая категория является дочерней текущему $parent */
        if ($row->parent_id == $parent) {

            print('<li>');

            /* Выводим название категории */
            print('<div>' . $row->name . '</div>');


            /* Вызываем рекурсивную функцию,
             чтобы вывести уже дочерние этой категории категории */
            print_list($categories, $row->id);


            print('</li>');

        }


    }


    /* Перебираем все продукты */
    foreach ($products as $product) {

        /* Если продукт относится к текущей категории, т.е. parent то вывести его название */
        /* В первом цикле parent = 0, поэтому выведутся продукты в корневой структуре */
        if ($product->category_id === $parent) {
            print('<li><div>' . $product->name . '</div></li>');
        }

    }

    print('</ul>');

}


/* Если запрос в бд вернул список категорий, то вызываем рекурсивную функцию */
if (count($categories)) {
    print_list($categories);
}


?>


</body>
</html>





