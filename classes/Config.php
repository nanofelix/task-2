<?php

/**
 * Created by PhpStorm.
 * User: astrofelix
 * Date: 04-Jun-18
 * Time: 23:22
 */
class Config
{

    public static function get($path = null)
    {
        if ($path) {
            $config = $GLOBALS['config'];
            $path = explode('/', $path);

            foreach ($path as $bit){
                if(isset($config[$bit])){
                    $config = $config[$bit];
                }
            }

            return $config;
        }

        return false;

    }

}